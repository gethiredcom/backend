import com.fasterxml.jackson.databind.ObjectMapper
import com.gethired.backend.controller.AuthenticationController
import com.gethired.backend.controller.CompanyAccessController
import com.gethired.backend.controller.UserController
import com.gethired.backend.dto.authentication.LoginRequestDto
import com.gethired.backend.dto.authentication.RegisterRequestDto
import com.gethired.backend.dto.user.GetUserResponseDto
import com.gethired.backend.dto.user.UpdateUserRequestDto
import com.gethired.backend.model.Application
import com.gethired.backend.model.Company
import com.gethired.backend.model.CompanyAccess
import com.gethired.backend.model.User
import com.gethired.backend.repository.CompanyAccessRepository
import com.gethired.backend.repository.CompanyRepository
import com.gethired.backend.repository.UserRepository
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import java.util.*

@SpringBootTest(classes = [CompanyAccessController::class])
@AutoConfigureMockMvc
class TestCompanyController {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockBean
    private lateinit var companyRepository: CompanyRepository

    @MockBean
    private lateinit var companyAccessRepository: CompanyAccessRepository

    private val objectMapper = ObjectMapper()

    @Test
    fun getCompanyAccesses_Status404() {
        val companyId = UUID.randomUUID()

        `when`(companyRepository.findById(companyId)).thenReturn(Optional.empty())

        mockMvc.perform(get("/api/company/{companyId}/access", companyId))
                .andExpect(status().isNotFound)
    }

    @Test
    fun createCompanyAccess_Status201() {
        val companyId = UUID.randomUUID()
        val companyAccess = CompanyAccess(UUID.randomUUID(), "test@example.com", "password")

        `when`(companyRepository.findById(companyId)).thenReturn(Optional.of(Company(companyId)))
        `when`(companyAccessRepository.save(companyAccess)).thenReturn(companyAccess)

        mockMvc.perform(post("/api/company/{companyId}/access", companyId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(companyAccess)))
                .andExpect(status().isCreated)
                .andExpect(content().string("Company access created successfully"))
    }

    @Test
    fun createCompanyAccess_InvalidEmailOrPassword_Status400() {
        val companyId = UUID.randomUUID()
        val invalidCompanyAccess = CompanyAccess(UUID.randomUUID(), "invalidemail", "")

        `when`(companyRepository.findById(companyId)).thenReturn(Optional.of(Company(companyId)))

        mockMvc.perform(post("/api/company/{companyId}/access", companyId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(invalidCompanyAccess)))
                .andExpect(status().isBadRequest)
    }

    @Test
    fun deleteCompanyAccess_Status200() {
        val companyAccessId = UUID.randomUUID()

        mockMvc.perform(delete("/api/company/{companyId}/access/{companyAccessId}", UUID.randomUUID(), companyAccessId))
                .andExpect(status().isOk)
    }
}
