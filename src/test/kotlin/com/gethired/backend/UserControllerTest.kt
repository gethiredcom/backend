import com.fasterxml.jackson.databind.ObjectMapper
import com.gethired.backend.controller.AuthenticationController
import com.gethired.backend.controller.UserController
import com.gethired.backend.dto.authentication.LoginRequestDto
import com.gethired.backend.dto.authentication.RegisterRequestDto
import com.gethired.backend.dto.user.GetUserResponseDto
import com.gethired.backend.dto.user.UpdateUserRequestDto
import com.gethired.backend.model.Application
import com.gethired.backend.model.User
import com.gethired.backend.repository.UserRepository
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import java.util.*

@SpringBootTest(classes = [UserController::class])
@AutoConfigureMockMvc
class UserControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockBean
    private lateinit var userRepository: UserRepository

    private val objectMapper = ObjectMapper()

    @Test
    fun getUserById_Status404() {
        val username = "nonexistentuser"

        `when`(userRepository.findById(username)).thenReturn(Optional.empty())

        mockMvc.perform(get("/api/users/{username}", username))
                .andExpect(status().isNotFound)
    }

    @Test
    fun updateUser_Status200() {
        val username = "testuser"
        val updateUserRequestDto = UpdateUserRequestDto("newusername")

        `when`(userRepository.findById(username)).thenReturn(Optional.of(User(username)))
        `when`(updateUserRequestDto.username?.let { userRepository.existsById(it) }).thenReturn(false)

        mockMvc.perform(put("/api/users/{username}", username)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateUserRequestDto)))
                .andExpect(status().isOk)
                .andExpect(content().string("Updates are saved successfully"))
    }

    @Test
    fun updateUser_UsernameTaken_Status400() {
        val username = "testuser"
        val updateUserRequestDto = UpdateUserRequestDto("existingusername")

        `when`(userRepository.findById(username)).thenReturn(Optional.of(User(username)))
        `when`(updateUserRequestDto.username?.let { userRepository.existsById(it) }).thenReturn(true)

        mockMvc.perform(put("/api/users/{username}", username)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateUserRequestDto)))
                .andExpect(status().isBadRequest)
                .andExpect(content().string("Username is taken"))
    }

    @Test
    fun deleteUser_Status200() {
        val username = "testuser"

        mockMvc.perform(delete("/api/users/{username}", username))
                .andExpect(status().isOk)
    }
}
