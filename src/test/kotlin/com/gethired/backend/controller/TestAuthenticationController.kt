package com.gethired.backend.controller

import com.gethired.backend.dto.authentication.LoginRequestDto
import com.gethired.backend.dto.authentication.RegisterRequestDto
import com.gethired.backend.model.User
import com.gethired.backend.repository.UserRepository
import com.gethired.backend.security.JWTProvider
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.ArgumentMatchers.any
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.crypto.password.PasswordEncoder

class TestAuthenticationController {

    @InjectMocks
    private lateinit var authenticationController: AuthenticationController

    @Mock
    private lateinit var userRepository: UserRepository

    @Mock
    private lateinit var passwordEncoder: PasswordEncoder

    @Mock
    private lateinit var authenticationManager: AuthenticationManager

    init {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `test successful registration`() {
        val registerRequestDto = RegisterRequestDto("testuser", "StrongPassword123")
        val encodedPassword = "encodedPassword"
        `when`(passwordEncoder.encode("StrongPassword123")).thenReturn(encodedPassword)

        val responseEntity = authenticationController.register(registerRequestDto)

        verify(userRepository).save(any(User::class.java))
        assertEquals("Registered successfully", responseEntity.body)
        assertEquals(HttpStatus.CREATED, responseEntity.statusCode)
    }

    @Test
    fun `test registration with invalid password`() {
        val registerRequestDto = RegisterRequestDto("testuser", "weak")
        val responseEntity = authenticationController.register(registerRequestDto)

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.statusCode)
    }

    @Test
    fun `test registration with existing username`() {
        val registerRequestDto = RegisterRequestDto("existingUser", "StrongPassword123")
        `when`(userRepository.existsById("existingUser")).thenReturn(true)

        val responseEntity = authenticationController.register(registerRequestDto)

        assertEquals("Username is taken", responseEntity.body)
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.statusCode)
    }

    @Test
    fun `test successful login`() {
        val loginRequestDto = LoginRequestDto("existingUser", "StrongPassword123")
        val authentication = UsernamePasswordAuthenticationToken("existingUser", "StrongPassword123")

        `when`(authenticationManager.authenticate(authentication)).thenReturn(authentication)

        val responseEntity = authenticationController.login(loginRequestDto)

        assertEquals(HttpStatus.OK, responseEntity.statusCode)
        responseEntity.body?.accessToken?.let { token ->
            val username = JWTProvider.usernameFromJWT(token)
            assertEquals(username, "existingUser")
        }
    }

    @Test
    fun `test login with incorrect credentials`() {
        val loginRequestDto = LoginRequestDto("existingUser", "OtherStrongPassword123")
        val authentication = UsernamePasswordAuthenticationToken("existingUser", "OtherStrongPassword123")

        `when`(authenticationManager.authenticate(authentication)).thenThrow(BadCredentialsException("Invalid credentials"))

        assertThrows<BadCredentialsException> { authenticationController.login(loginRequestDto) }
    }

    @Test
    fun `test login with missing username`() {
        val loginRequestDto = LoginRequestDto("", "StrongPassword123")
        val responseEntity = authenticationController.login(loginRequestDto)

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.statusCode)
    }

    @Test
    fun `test login with missing password`() {
        val loginRequestDto = LoginRequestDto("testuser", "")
        val responseEntity = authenticationController.login(loginRequestDto)

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.statusCode)
    }
}