package com.gethired.backend.controller

import com.gethired.backend.dto.authentication.RegisterRequestDto
import com.gethired.backend.model.User
import com.gethired.backend.repository.UserRepository
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.any
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import org.springframework.http.HttpStatus
import org.springframework.security.crypto.password.PasswordEncoder

class TestUserController {
    @InjectMocks
    private lateinit var authenticationController: AuthenticationController

    @InjectMocks
    private lateinit var userController: UserController

    @Mock
    private lateinit var userRepository: UserRepository

    @Mock
    private lateinit var passwordEncoder: PasswordEncoder

    init {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `test finding user`() {
        val registerRequestDto = RegisterRequestDto("testuser", "StrongPassword123")
        val encodedPassword = "encodedPassword"
        `when`(passwordEncoder.encode("StrongPassword123")).thenReturn(encodedPassword)

        authenticationController.register(registerRequestDto)

        verify(userRepository).save(any(User::class.java))

        val temp = userRepository.findAll()

        val responseEntity = userController.getUserByUsername("testuser");
        Assertions.assertEquals(HttpStatus.OK, responseEntity.statusCode)
    }


}