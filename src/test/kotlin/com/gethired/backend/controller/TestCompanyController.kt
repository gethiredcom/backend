package com.gethired.backend.controller

import com.gethired.backend.dto.company.CreateCompanyRequestDto
import com.gethired.backend.dto.company.UpdateCompanyRequestDto
import com.gethired.backend.model.Company
import com.gethired.backend.repository.CompanyRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.any
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.springframework.http.HttpStatus
import java.util.*

class TestCompanyController {
    @InjectMocks
    private lateinit var companyController: CompanyController

    @Mock
    private lateinit var companyRepository: CompanyRepository

    init {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `test successful company creation`() {
        val createCompanyRequestDto = CreateCompanyRequestDto(
            "testcompany",
            "testdescription",
            10
        )

        val responseEntity = companyController.createCompany(createCompanyRequestDto)

        verify(companyRepository).save(any(Company::class.java))
        assertEquals("Company created successfully", responseEntity.body)
        assertEquals(HttpStatus.CREATED, responseEntity.statusCode)
    }

    @Test
    fun `test company creation with invalid name`() {
        val createCompanyRequestDto = CreateCompanyRequestDto(
            "a",
            "testdescription",
            10
        )

        val responseEntity = companyController.createCompany(createCompanyRequestDto)

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.statusCode)
    }

    @Test
    fun `test company update`() {
        val createCompanyRequestDto = CreateCompanyRequestDto(
            "testcompany",
            "testdescription",
            10
        )
        companyController.createCompany(createCompanyRequestDto)
        val createdCompany = companyRepository.findByName("testcompany");

        val updateCompanyRequestDto = UpdateCompanyRequestDto(
            "newname",
            "newdescription",
            20
        )

        val responseEntity = createdCompany?.let { companyController.updateCompany(it.id, updateCompanyRequestDto) }

        verify(companyRepository).save(any(Company::class.java))
        if (responseEntity != null) {
            assertEquals("Company updated successfully", responseEntity.body)
        }
        if (responseEntity != null) {
            assertEquals(HttpStatus.OK, responseEntity.statusCode)
        }
    }

    @Test
    fun `test updating company with invalid name`() {
        val createCompanyRequestDto = CreateCompanyRequestDto(
            "testcompany1",
            "testdescription",
            10
        )
        companyController.createCompany(createCompanyRequestDto)
        val createdCompany = companyRepository.findByName("testcompany1");

        val updateCompanyRequestDto = UpdateCompanyRequestDto(
            "a",
            "desc",
            10
        )

        val responseEntity = createdCompany?.let { companyController.updateCompany(it.id, updateCompanyRequestDto) }
        verify(companyRepository).save(any(Company::class.java))
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity?.statusCode)
    }

}