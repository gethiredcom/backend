import com.fasterxml.jackson.databind.ObjectMapper
import com.gethired.backend.controller.AuthenticationController
import com.gethired.backend.dto.authentication.LoginRequestDto
import com.gethired.backend.dto.authentication.RegisterRequestDto
import com.gethired.backend.repository.UserRepository
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import java.util.*

@SpringBootTest(classes = [AuthenticationController::class])
@AutoConfigureMockMvc
class AuthenticationControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockBean
    private lateinit var authenticationManager: AuthenticationManager

    @MockBean
    private lateinit var userRepository: UserRepository

    @MockBean
    private lateinit var passwordEncoder: PasswordEncoder

    private val objectMapper = ObjectMapper()

    @Test
    fun registerStatus400() {
        mockMvc.perform(post("/api/auth/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(RegisterRequestDto("", ""))))
                .andExpect(status().isBadRequest)
    }

    @Test
    fun registerStatus201() {
        `when`(userRepository.existsById(Mockito.anyString())).thenReturn(false)
        `when`(passwordEncoder.encode(Mockito.anyString())).thenReturn("encodedPassword")

        mockMvc.perform(post("/api/auth/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(RegisterRequestDto("username", "password"))))
                .andExpect(status().isCreated)
    }

    @Test
    fun loginStatus200() {
        val loginRequestDto = LoginRequestDto("username", "password")

        mockMvc.perform(post("/api/auth/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequestDto)))
                .andExpect(status().isOk)
    }
}
