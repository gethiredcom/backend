package com.gethired.backend.dto.user

data class GetUserResponseDto(
    val username: String,
)