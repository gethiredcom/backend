package com.gethired.backend.dto.company

data class UpdateCompanyRequestDto(
    val name: String? = null,
    val description: String? = null,
    val companySize: Int? = null,
    val logoUrl: String? = null,
)