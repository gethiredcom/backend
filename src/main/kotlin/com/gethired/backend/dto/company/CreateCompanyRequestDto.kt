package com.gethired.backend.dto.company

data class CreateCompanyRequestDto(
    val name: String,
    val description: String,
    val companySize: Int,
)
