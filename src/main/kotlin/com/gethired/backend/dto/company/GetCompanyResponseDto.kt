package com.gethired.backend.dto.company

data class GetCompanyResponseDto(
    val name: String,
    val description: String,
    val companySize: Int,
    val logoUrl: String,
)