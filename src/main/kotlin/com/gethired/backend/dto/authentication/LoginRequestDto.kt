package com.gethired.backend.dto.authentication

data class LoginRequestDto(
    val username: String,
    val password: String,
)