package com.gethired.backend.dto.authentication

data class RegisterRequestDto(
    val username: String,
    val password: String,
)
