package com.gethired.backend.dto.authentication

data class LoginResponseDto(
    val accessType: String = "Bearer",
    val accessToken: String,
)
