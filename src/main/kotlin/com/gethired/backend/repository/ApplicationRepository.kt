package com.gethired.backend.repository

import com.gethired.backend.model.Application
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface ApplicationRepository : JpaRepository<Application, UUID>