package com.gethired.backend.repository

import com.gethired.backend.model.CompanyAccess
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface CompanyAccessRepository : JpaRepository<CompanyAccess, UUID>