package com.gethired.backend.repository

import com.gethired.backend.model.Company
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface CompanyRepository : JpaRepository<Company, UUID> {
    fun findByName(name: String): Company?
}