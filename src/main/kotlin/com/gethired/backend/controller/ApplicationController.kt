package com.gethired.backend.controller

import com.gethired.backend.repository.ApplicationRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/application")
class ApplicationController {

    @Autowired
    lateinit var applicationRepository: ApplicationRepository

}