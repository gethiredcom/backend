package com.gethired.backend.controller

import com.gethired.backend.dto.authentication.LoginRequestDto
import com.gethired.backend.dto.authentication.LoginResponseDto
import com.gethired.backend.dto.authentication.RegisterRequestDto
import com.gethired.backend.model.User
import com.gethired.backend.repository.UserRepository
import com.gethired.backend.security.JWTProvider
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/auth", consumes = [MediaType.APPLICATION_JSON_VALUE])
class AuthenticationController {

    @Autowired
    lateinit var authenticationManager: AuthenticationManager

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var passwordEncoder: PasswordEncoder

    @PostMapping("/register")
    fun register(@RequestBody registerRequestDto: RegisterRequestDto): ResponseEntity<String> {
        val badRequest = !Validator.validPassword(registerRequestDto.password)
        if (badRequest) return ResponseEntity.badRequest().build()

        if (userRepository.existsById(registerRequestDto.username)) {
            return ResponseEntity("Username is taken", HttpStatus.BAD_REQUEST)
        }

        val user = User(
            username = registerRequestDto.username,
            password = passwordEncoder.encode(registerRequestDto.password),
        )
        userRepository.save(user)

        return ResponseEntity("Registered successfully", HttpStatus.CREATED)
    }

    @PostMapping("/login")
    fun login(@RequestBody loginRequestDto: LoginRequestDto): ResponseEntity<LoginResponseDto> {
        val badRequest = loginRequestDto.username.isBlank() // TODO: Create a Regex for username validation
                || !Validator.validPassword(loginRequestDto.password)

        if (badRequest) {
            return ResponseEntity.badRequest().build()
        }

        val authentication = authenticationManager.authenticate(
            UsernamePasswordAuthenticationToken(loginRequestDto.username, loginRequestDto.password)
        )

        SecurityContextHolder.getContext().authentication = authentication

        val token = JWTProvider.provide(authentication)

        val response = LoginResponseDto(accessToken = token)
        return ResponseEntity(response, HttpStatus.OK)
    }

}