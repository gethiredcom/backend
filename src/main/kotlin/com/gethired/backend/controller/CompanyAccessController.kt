package com.gethired.backend.controller

import com.gethired.backend.model.CompanyAccess
import com.gethired.backend.repository.CompanyAccessRepository
import com.gethired.backend.repository.CompanyRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/api/company/{companyId}/access")
class CompanyAccessController {

    @Autowired
    lateinit var companyRepository: CompanyRepository

    @Autowired
    lateinit var companyAccessRepository: CompanyAccessRepository

    @GetMapping("")
    fun getCompanyAccesses(@PathVariable companyId: UUID): ResponseEntity<List<CompanyAccess>> {
        val result = companyRepository.findById(companyId)
        if (!result.isPresent) return ResponseEntity.notFound().build()

        val accesses = result.get().companyAccesses.toList()
        return ResponseEntity.ok(accesses)
    }

    @PostMapping("")
    fun createCompanyAccess(
        @PathVariable companyId: UUID,
        @RequestBody companyAccess: CompanyAccess,
    ): ResponseEntity<String> {
        val result = companyRepository.findById(companyId)
        if (!result.isPresent) return ResponseEntity.notFound().build()

        val badRequest = !Validator.validEmail(companyAccess.email)
                || !Validator.validPassword(companyAccess.password)
        if (badRequest) return ResponseEntity.badRequest().build()

        val companyAccess = companyAccess.copy(company = result.get())

        companyAccessRepository.save(companyAccess)
        return ResponseEntity("Company access created successfully", HttpStatus.CREATED)
    }

    @DeleteMapping("/{companyAccessId}")
    fun deleteCompanyAccess(@PathVariable companyAccessId: UUID) = companyAccessRepository.deleteById(companyAccessId)

}