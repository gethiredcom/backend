package com.gethired.backend.filter

import jakarta.servlet.FilterChain
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.springframework.core.annotation.Order
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter


@Component
@Order(2)
class ResourceOwnerFilter : OncePerRequestFilter() {

    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        val authentication = SecurityContextHolder.getContext().authentication
        val requesterUsername = authentication.name
        val username = usernameFromRequest(request)

        if (requesterUsername != username) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED)
        }

        filterChain.doFilter(request, response)
    }

    override fun shouldNotFilter(request: HttpServletRequest): Boolean {
        val routePatterns = mapOf(
            "/api/users/.*/applications".toRegex() to listOf("GET"),
            "/api/users/.*".toRegex() to listOf("PUT", "DELETE"),
        )

        return routePatterns.map { (pattern, methods) ->
            println("${request.requestURI.matches(pattern)}, ${request.method in methods}")
            request.requestURI.matches(pattern) && request.method in methods
        }.none { it }
    }

    fun usernameFromRequest(request: HttpServletRequest): String? {
        val withoutPrefix = request.requestURI.removePrefix("/api/users/")
        val nextSlashIndex = withoutPrefix.indexOfFirst { it == '/' }
        return when (nextSlashIndex) {
            -1 -> withoutPrefix
            else -> withoutPrefix.substring(0, nextSlashIndex)
        }
    }

}