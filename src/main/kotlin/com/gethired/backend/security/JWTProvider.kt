package com.gethired.backend.security

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import org.springframework.security.core.Authentication
import java.util.*

object JWTProvider {
    private val publicKey = RSAReader.readPublicKey("gethired-public.der")
    private val privateKey = RSAReader.readPrivateKey("gethired-private.der")
    private val algorithm = Algorithm.RSA512(publicKey, privateKey)

    fun provide(authentication: Authentication): String {
        val username = authentication.name
        val issuedAt = Date()
        val expiresAt = Date(issuedAt.time + SecurityConstants.VALIDITY_TIME)

        return JWT.create()
            .withSubject(username)
            .withIssuedAt(issuedAt)
            .withExpiresAt(expiresAt)
            .sign(algorithm)
    }

    fun usernameFromJWT(token: String): String = JWT.decode(token)
        .getClaim("sub").asString()
}