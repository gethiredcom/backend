package com.gethired.backend.security

import com.gethired.backend.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Component

@Component
class CustomUserDetailsService : UserDetailsService {
    @Autowired
    lateinit var userRepository: UserRepository

    override fun loadUserByUsername(username: String): UserDetails {
        val result = userRepository.findById(username)
        if (!result.isPresent) {
            throw UsernameNotFoundException("User not found")
        }

        val user = result.get()
        return User(user.username, user.password, emptyList())
    }
}
