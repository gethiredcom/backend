package com.gethired.backend.model

import jakarta.persistence.*
import java.util.*

@Entity
@Table(name = "vacancies")
data class Vacancy(

    @Id
    val id: UUID = UUID.randomUUID(),

    val title: String = "",

    val description: String = "",

    // Salary range

    @ManyToMany(cascade = [CascadeType.ALL])
    @JoinTable(
        name = "vacancies_skill_tags",
        joinColumns = [JoinColumn(name = "vacancy_id")],
        inverseJoinColumns = [JoinColumn(name = "skill_tag_id")]
    )
    val skillTags: MutableSet<SkillTag> = mutableSetOf(),

    @OneToMany(cascade = [CascadeType.ALL])
    @JoinColumn(name = "vacancy_id", referencedColumnName = "id")
    val applications: MutableSet<Application> = mutableSetOf(),

)
