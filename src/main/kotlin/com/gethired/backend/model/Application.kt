package com.gethired.backend.model

import jakarta.persistence.*
import java.util.*

@Entity
@Table(name = "applications")
data class Application(

    @Id
    val id: UUID = UUID.randomUUID(),

    @ManyToOne(fetch = FetchType.LAZY)
    val user: User = User(),

    @ManyToOne(fetch = FetchType.LAZY)
    val vacancy: Vacancy = Vacancy(),

    // NOTE: Use ApplicationStatus.[STATUS].name
    val status: String = "",

)

enum class ApplicationStatus(val displayName: String) {
    SUBMITTED("Submitted"),
    UNDER_REVIEW("Under review"),
    SELECTED_FOR_INTERVIEW("Selected for interview"),
    OFFER_ACCEPTED("Offer accepted"),
    OFFER_DECLINED("Offer declined"),
    REJECTED("Rejected"),
}