package com.gethired.backend.model

import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.ManyToMany
import jakarta.persistence.Table

@Entity
@Table(name = "skill_tags")
data class SkillTag(

    @Id
    val name: String = "",

    @ManyToMany(mappedBy = "skillTags")
    val vacancies: MutableSet<Vacancy> = mutableSetOf(),

)
