package com.gethired.backend.model

import jakarta.persistence.*

@Entity
@Table(name = "users")
data class User(

    @Id
    val username: String = "",

    val password: String = "",

    @OneToMany(cascade = [CascadeType.ALL])
    @JoinColumn(name = "user_username", referencedColumnName = "username")
    val applications: MutableSet<Application> = mutableSetOf(),

)
