package com.gethired.backend.model

import jakarta.persistence.*
import java.util.*

@Entity
@Table(name = "company_accesses")
data class CompanyAccess(

    @Id
    val id: UUID = UUID.randomUUID(),

    val email: String = "",

    val password: String = "",

    @ManyToOne(fetch = FetchType.LAZY)
    val company: Company = Company()

)
